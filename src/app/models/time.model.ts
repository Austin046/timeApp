import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export interface Question {
    time_to_display: string,
    options: string[]
}

export interface TimerQuestion {
    version: string,
    questions: Question[]
}

export interface State extends EntityState<Question> {
    
}