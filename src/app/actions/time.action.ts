import { Action } from '@ngrx/store';
import * as timeModel from '../models/time.model';

export const Fetch_Questions         = '[Time] fetch Questions';
export const Fetch_Questions_Failure = '[Time] fetch Questions Failure';
export const Fetch_Questions_Success = '[Time] fetch Questions Success';
export const Submit_Option           = '[Time] submit Option';


export class fetchQuestions implements Action {
    readonly type = Fetch_Questions;
    constructor() {}
}

export class fetchQuestionsFailure implements Action {
    readonly type = Fetch_Questions_Failure;
    constructor(public error: any) {}
}

//if fetch data success , pass questions into state
export class fetchQuestionsSuccess implements Action {
    readonly type = Fetch_Questions_Success;
    constructor(public payload: timeModel.TimerQuestion) {}
}

export class submitOption implements Action {
    readonly type = Submit_Option;
    constructor(public playload: boolean) {}
}

export type Actions
    = fetchQuestions
    | fetchQuestionsFailure
    | fetchQuestionsSuccess
    | submitOption