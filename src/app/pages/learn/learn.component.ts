import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../reducers/time.reducer';
import { Observable } from 'rxjs/Observable';
import { Question } from '../../models/time.model';
import * as timeActions from '../../actions/time.action';
import { Router } from '@angular/router';

@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.css']
})
export class LearnComponent implements OnInit {

  currentScore: number
  currentQuestionIndex: number
  questionLength: number
  currentQuestion: Question

  timeToDisplay: Observable<string>


  canvas :any
  ctx
  radius
  
  


  constructor(private store: Store<State>, private router: Router) { }
  submit(opt: string) {
    console.log(opt);
    if(this.currentQuestionIndex === this.questionLength) {
      console.log('end');
      this.store.dispatch(new timeActions.fetchQuestions());
    } else {
      this.store.dispatch(new timeActions.submitOption(opt === this.currentQuestion.time_to_display));
    }
  }

  ngAfterViewInit() {
    
  }

  ngOnInit() {

    this.canvas = document.getElementById("canvas");
    this.ctx = this.canvas.getContext("2d");
    this.radius = this.canvas.height / 2;
    this.ctx.translate(this.radius, this.radius);
    this.radius = this.radius * 0.90

   

    let currentState: Observable<any> = this.store.select(state => state);
    currentState.subscribe(
      (data) => {
        //urly code
        console.log('isSuccess:', data.time)
        this.currentScore = data.time.score
        this.currentQuestionIndex = data.time.currentQuestion + 1
        this.questionLength = data.time.questions.length
        this.currentQuestion = data.time.questions[data.time.currentQuestion]
        this.timeToDisplay = data.time.questions[data.time.currentQuestion].time_to_display

        drawClock(this.timeToDisplay, this.ctx, this.radius)
        
        console.log(this.currentScore, this.currentQuestionIndex, this.questionLength, this.currentQuestion)
      }
    )

    //get current question


    console.log('Learn is loaded')
   
    // setInterval(drawClock, 1000);
    // drawClock('12:06');
    // drawClock(this.timeToDisplay);

    


    
  }

  

}
function drawClock(timeString, ctx, radius) {
  drawFace(ctx, radius);
  drawNumbers(ctx, radius);
  drawTime(ctx, radius, timeString);
}

function drawFace(ctx, radius) {
  var grad;
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI);
  ctx.fillStyle = 'white';
  ctx.fill();
  grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
  grad.addColorStop(0, '#333');
  grad.addColorStop(0.5, 'white');
  grad.addColorStop(1, '#333');
  ctx.strokeStyle = grad;
  ctx.lineWidth = radius*0.1;
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
  ctx.fillStyle = '#333';
  ctx.fill();
}

function drawNumbers(ctx, radius) {
  var ang;
  var num;
  ctx.font = radius*0.15 + "px arial";
  ctx.textBaseline="middle";
  ctx.textAlign="center";
  for(num = 1; num < 13; num++){
    ang = num * Math.PI / 6;
    ctx.rotate(ang);
    ctx.translate(0, -radius*0.85);
    ctx.rotate(-ang);
    ctx.fillText(num.toString(), 0, 0);
    ctx.rotate(ang);
    ctx.translate(0, radius*0.85);
    ctx.rotate(-ang);
  }
}

function drawTime(ctx, radius, timeString: string = '12:05'){
  // var now = new Date();
  // // var now = new Date(1510027320000);
  // var hour = now.getHours();
  // var minute = now.getMinutes();
  // var second = now.getSeconds();

  let h = timeString.substring(0,2);
  let m = timeString.substring(3,6);
  let i_h = parseInt(h);
  let i_m = parseInt(m);
  var hour = i_h; 
  var minute = i_m;
  var second = 0;

  //hour
  hour=hour%12;
  hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
  drawHand(ctx, hour, radius*0.5, radius*0.07);
  //minute
  minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
  drawHand(ctx, minute, radius*0.8, radius*0.07);
  // second
  // second=(second*Math.PI/30);
  // drawHand(ctx, second, radius*0.9, radius*0.02);
}

function drawHand(ctx, pos, length, width) {
  ctx.beginPath();
  ctx.lineWidth = width;
  ctx.lineCap = "round";
  ctx.moveTo(0,0);
  ctx.rotate(pos);
  ctx.lineTo(0, -length);
  ctx.stroke();
  ctx.rotate(-pos);
}