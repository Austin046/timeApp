import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading.component';
import { LoadingRoutingModule } from './loading-routing.module';

@NgModule({
  imports: [
    CommonModule,
    LoadingRoutingModule
  ],
  declarations: [LoadingComponent],

})
export class LoadingModule { }
