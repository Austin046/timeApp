import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './reducers/time.reducer';
import * as timeActions from './actions/time.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  constructor(public store: Store<State>) {
    console.log('App Component: Constructor dispatch fetchQuestion')
    this.store.dispatch(new timeActions.fetchQuestions());
  }

  ngOnInit() {
    
  }
  
  ngAfterViewInit() {
    
    
  }

}
