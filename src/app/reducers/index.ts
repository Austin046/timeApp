import { ActionReducerMap } from '@ngrx/store';

import * as fromTime from '../reducers/time.reducer';
import { createSelector } from '@ngrx/store/src/selector';

export interface State {
    time: fromTime.State
}

export const reducer: ActionReducerMap<State> = {
    time: fromTime.reducers
}   
