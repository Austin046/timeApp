import { Question } from '../models/time.model';
import * as actions from '../actions/time.action';

export interface State {
    isFetching: boolean,
    isSuccess: boolean,
    version: string,
    questions: Question[],
    score: number,
    currentQuestion: number
}
export const initialState = {
    isFetching: false,
    isSuccess: false,
    version: '',
    questions: [],
    score: 0,
    currentQuestion: 0
}

export function reducers(state: State = initialState, action: actions.Actions): State {
    switch(action.type) {
        case actions.Fetch_Questions:
            return { ...state, isFetching: true,  isSuccess: false }
        case actions.Fetch_Questions_Failure: 
            return { ...state, isFetching: false, isSuccess: false }
        case actions.Fetch_Questions_Success:  {
            console.log('fetched data: ', action.payload);
            return { ...state, isFetching: false, isSuccess: true, version: action.payload.version, questions: action.payload.questions, score: 0, currentQuestion: 0 }
        }
        case actions.Submit_Option: 
            return action.playload 
                ? { ...state, score: state.score + 1, currentQuestion: state.currentQuestion+1 } 
                : { ...state, currentQuestion: state.currentQuestion + 1 }
            
        
            
        default:
            return { ...state }
    }
}