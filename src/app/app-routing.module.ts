import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoadingComponent } from './pages/loading/loading.component';
import { LearnComponent } from './pages/learn/learn.component';


export const routes: Routes = [
  { path: 'learn', loadChildren: './pages/learn/learn.module#LearnModule' },
  { path: '',      loadChildren: './pages/loading/loading.module#LoadingModule'  },
  
 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
