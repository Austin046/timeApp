import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/delay';

import { url } from '../configs/constant';
import * as timeActions from '../actions/time.action';

let dataResult = {"version":"1.0","questions":[{"time_to_display":"10:31","options":["10:59","13:18","10:31"]},{"time_to_display":"21:16","options":["03:34","09:16","21:16"]},{"time_to_display":"21:34","options":["15:31","03:17","21:34"]},{"time_to_display":"12:35","options":["12:35","16:10","17:18"]},{"time_to_display":"17:35","options":["05:08","07:16","17:35"]},{"time_to_display":"09:01","options":["16:18","22:21","09:01"]},{"time_to_display":"12:45","options":["12:12","18:45","12:45"]},{"time_to_display":"09:53","options":["16:13","09:53","03:46"]},{"time_to_display":"03:55","options":["00:13","20:24","03:55"]},{"time_to_display":"11:14","options":["11:43","00:25","11:14"]},{"time_to_display":"03:55","options":["12:30","03:55","01:53"]}]}


@Injectable()
export class TimeEffects {
    constructor(private http: HttpClient, private router: Router,  private action$: Actions ) {}

    // function isSuccessObservable(milisecons) {
    //     return new Observable().delay(milisecons)
    // }
    

    @Effect()
    fetchQuestion$ = this.action$
        //listen for the fect question action
        .ofType(timeActions.Fetch_Questions)
        .map(() => {
                this.router.navigate(['/learn']);
                console.log('fectchQuestion Effect dispatch')
                return new timeActions.fetchQuestionsSuccess(dataResult);
            }           
        )
        // .takeUntil(
        //     new Observable().delay(5000)
        // )
  
        // .switchMap(
        //     action => 
        //         this.http.get(url)
        //             .takeUntil(
        //                 new Observable().delay(5000)
        //                     .map(() => new timeActions.fetchQuestionsFailure(null))
        //             )
        //             .map(data => new timeActions.fetchQuestionsSuccess(dataResult))
        //             .catch( (err) => of(new timeActions.fetchQuestionsFailure(err)))
        // )
    // @Effect({dispatch: false})
    // fetchQuestionSuccess$ = this.action$
    //     .ofType(timeActions.Fetch_Questions_Success)
    //     .do(() => { 
    //         this.router.navigate(['/']); 
    //     })

    // @Effect({dispatch: false})
    // fetchQuestionFailure$ = this.action$
    //     .ofType(timeActions.Fetch_Questions_Failure)
    //     .do(() => {
    //          this.router.navigate(['/']); 
    //         })
}